require("dotenv").config();
const HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',

  networks: {
    development: {
      host: "127.0.0.1", // Localhost (default: none)
      port: 8545, // Standard Ethereum port (default: none)
      network_id: "*", // Any network (default: none)
    },
    kovan: {
      provider: function () {
        return new HDWalletProvider(
          process.env.PRIVATE_KEY || "",
          process.env.WS_URI || "" //process.env.HTTPS_URI || ""
        );
      },
      network_id: 42,
      gas: 4500000,
      gasPrice: 10000000000,
    },
    rinkeby: {
      provider: function () {
        return new HDWalletProvider(
          process.env.PRIVATE_KEY || "",
          process.env.WS_URI || "" //process.env.HTTPS_URI || ""
        );
      },
      network_id: 4,
      gas: 4500000,
      gasPrice: 10000000000,
    },
  },
  compilers: {
    solc: {
      version: "0.8.0", // TODO: Adjust solidity version based on Chainlink
    },
  },
  db: {
    enabled: false,
  },
};