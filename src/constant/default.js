export const defaultPairs = {
  from: {
    symbol: "LINK",
    name: "chainlink",
    amount: "",
    rates: "",
    balance: 0,
  },
  to: {
    symbol: "BAT", // Token's symbol
    name: "basic-attention-token", // Token's name
    amount: "", // Token's amount to receive or send
    rates: "", // Token's exchange rates
    balance: 0, // Token's balance
  },
};
