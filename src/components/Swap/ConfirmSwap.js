import React, { useState, useEffect, useContext, useRef } from "react";
import PropTypes from "prop-types";
import { Message, Button, Dimmer, Loader, Segment } from "semantic-ui-react";
import Countdown from "react-countdown";
import { NotificationManager } from "react-notifications";
import Page from "../Layout/Page";
import tokens from "../../data/tokens.json";
import { Web3Context } from "../../context/Web3Context";
import { getLatestPriceResponse, getPrice } from "../../helpers/linkPrice";
import { toNumber } from "../../helpers/decimal";
import { swap } from "../../helpers/dex";

const ConfirmSwap = ({ requestQuote, quote }) => {
  const { from, to } = quote;
  const [price, setPrice] = useState(null);
  const [rates, setRates] = useState(null);
  const [loading, setLoading] = useState({ isLoading: false, text: "" });
  const [countdown, setCountdown] = useState(10);
  const refCountdown = useRef(null);

  const web3State = useContext(Web3Context);

  // Recalculate the quote
  useEffect(() => {
    const requote = async () => {
      setLoading({ isLoading: true, text: "Preparing Quote..." });
      const response = getLatestPriceResponse(
        tokens[from.name].to_usd.contract,
        tokens[to.name].to_usd.contract
      );
      response.then((data) => {
        if (data) {
          const result = getPrice(from.amount, data);
          setPrice(toNumber(result.price));
          setRates(toNumber(result.rates));
        }
        setLoading({ isLoading: false, text: "" });
      });
    };

    // Get the latest exchange price
    requote();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = async () => {
    refCountdown.current.stop();
    setLoading({ isLoading: true, text: "Processing..." });
    
    // Swap
    swap(
      web3State.web3,
      web3State.account,
      web3State.dex,
      from.name,
      to.name,
      from.amount,
      price,
      setLoading
    ).then(result => {
      if (result) {
        setLoading({ isLoading: false, text: "" });
        NotificationManager.success(
          `You received ${price} ${to.symbol}`,
          "Success!",
          3000
        );
        requestQuote(false); // reset to swap page
      } else {
        setLoading({ isLoading: false, text: "" });
        NotificationManager.error("Transaction failed :(", "Oops", 3000);
        requestQuote(false); // reset to swap page
      }
    })
  };

  return (
    <Page title="We secure it for you!">
      <Segment className="segment">
        <Dimmer active={loading.isLoading}>
          <Loader inverted content={loading.text} />
        </Dimmer>
        <Message warning compact>
          <Message.Header>
            You have{" "}
            {
              <Countdown
                ref={refCountdown} 
                date={Date.now() + countdown * 1000}
                renderer={({ seconds }) => <span>{seconds}</span>}
                onTick={({ seconds }) => setCountdown(seconds)}
                onComplete={() => requestQuote(false)}
              />
            }
            s to complete transaction!
          </Message.Header>
        </Message>
        <div className="column" style={{ marginBottom: 16 }}>
          <span className="input-label">You will send:</span>
          <span
            className="input-label"
            style={{ fontSize: "2rem", margin: "8px 0" }}
          >
            {from.amount} {from.symbol}
          </span>
          <BalanceLabel
            amount={from.balance - from.amount}
            token={from.symbol}
          />
        </div>
        <div className="column" style={{ marginBottom: 16 }}>
          <span className="input-label">You will receive:</span>
          <span
            className="input-label"
            style={{ fontSize: "2rem", margin: "8px 0" }}
          >
            {price} {to.symbol}
          </span>
          <BalanceLabel amount={to.balance + price} token={to.symbol} />
        </div>
        <div>
          <p className="input-sublabel">
            Price: {from.rates} {from.symbol} ≈ {rates} {to.symbol}
          </p>
        </div>
        <div>
          <Button
            circular
            compact
            style={{ margin: "8px 0" }}
            className="purple"
            content="Confirm Swap"
            icon="right arrow"
            labelPosition="right"
            onClick={handleSubmit}
          />
          <Button
            circular
            compact
            style={{ margin: "8px 16px 0" }}
            className="grey"
            content="Cancel"
            onClick={() => requestQuote(false)}
          />
        </div>
      </Segment>
    </Page>
  );
};

const BalanceLabel = ({ amount, token }) => (
  <div className="input-sublabel">
    <label>
      New Balance: {amount} {token}
    </label>
  </div>
);

BalanceLabel.propTypes = {
  amount: PropTypes.number || PropTypes.string,
  token: PropTypes.string,
};

export default ConfirmSwap;
