import React, { useState, createContext } from "react";

export const Web3Context = createContext();
export const Web3ActionsContext = createContext();

const Web3Provider = (props) => {
  const [web3State, setWeb3State] = useState({
    web3: null,
    account: null,
    networkId: null,
    dex: null,
  });

  return (
    <Web3Context.Provider value={web3State}>
      <Web3ActionsContext.Provider value={setWeb3State}>
        {props.children}
      </Web3ActionsContext.Provider>
    </Web3Context.Provider>
  );
};

export default Web3Provider;
