import ERC20 from "../abis/ERC20.json";

// This function calls allowance() of ERC20 contract
// It returns allowance amount if success, else -1
export const getTokenAllowance = async (web3, account, tokenAddress) => {
  const erc20Instance = new web3.eth.Contract(
    ERC20.abi, // Smart contract abi
    tokenAddress // Smart Contract address
  );

  try {
    let result = await erc20Instance.methods
      .allowance(account, process.env.REACT_APP_CONTRACT_ADDRESS)
      .call();
    if (result) {
      return result;
    }
    return -1;
  } catch (e) {
    console.log("getAllowance error:", e);
    return -1;
  }
};

// This function gives permission for the contract to transfer up to a certain amount of user token
// It returns true if success, else false
export const approveToken = async (web3, account, tokenAddress, amount) => {
  const erc20Instance = new web3.eth.Contract(
    ERC20.abi, // Smart contract abi
    tokenAddress // Smart Contract address
  );

  try {
    let result = await erc20Instance.methods
      .approve(process.env.REACT_APP_CONTRACT_ADDRESS, amount)
      .send({ from: account });
    if (result) {
      if (result.status) {
        return true;
      }
    }
    return false;
  } catch (e) {
    console.log("Approve error:", e);
    return false;
  }
};
