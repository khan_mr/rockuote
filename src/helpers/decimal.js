import { decimalFactor } from "../constant/number";
import { BigNumber } from "bignumber.js";

// This function formats a number with N decimal places
// Example: N = 3 -> 1.45678 = 1.456, 9.8 = 9.8, 7 -> 7, 0 -> 0
export const formatDecimal = (input, place = 5) => {
  const digit = String.raw`${place}`;
  const pattern = new RegExp("^-?\\d+(?:\\.\\d{0,"+ digit +"})?");
  let res = input.toString().match(pattern)[0];
  return Number(res);
};

// This function convert from big number to small number
export const toNumber = (amount) => {
  const x = new BigNumber(amount);
  const y = x.dividedBy(decimalFactor).toNumber();
  return formatDecimal(y);
};

// This function multiplies amount by decimal factor and convert it to big number
// Example: amount = 1.23 -> 1230000000000000000 -> '1230000000000000000'
export const toBigNumber = (amount) => {
  return toFixed(amount * decimalFactor).toString();
};

// This function removes scientific notation for large number
// Source: https://stackoverflow.com/questions/1685680/how-to-avoid-scientific-notation-for-large-numbers-in-javascript
const toFixed = (x) => {
  if (Math.abs(x) < 1.0) {
    let e = parseInt(x.toString().split('e-')[1]);
    if (e) {
        x *= Math.pow(10,e-1);
        x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
    }
  } else {
    let e = parseInt(x.toString().split('+')[1]);
    if (e > 20) {
        e -= 20;
        x /= Math.pow(10,e);
        x += (new Array(e+1)).join('0');
    }
  }
  return x;
}