import aggregatorV3Interface from "../abis/AggregatorV3Interface.json";
import {decimalFactor} from "../constant/number";
import Web3 from "web3";

// This function uses Chainlink Price Feed library 
// It returns a response from Chainlink if success, else null
export const getLatestPriceResponse = (contract1, contract2) => {
  try {
    const web3 = new Web3(
      `https://kovan.infura.io/v3/${process.env.REACT_APP_INFURA_ID}`
    );

    const addr1 = contract1;
    const addr2 = contract2;
    const priceFeed1 = new web3.eth.Contract(aggregatorV3Interface.abi, addr1);
    const priceFeed2 = new web3.eth.Contract(aggregatorV3Interface.abi, addr2);

    const promise1 = priceFeed1.methods.latestRoundData().call();
    const promise2 = priceFeed2.methods.latestRoundData().call();

    return Promise.all([promise1, promise2]);
  } catch (e) {
    console.log("Link Price error: ", e);
    return null;
  }
};

// This function calculate the exchange price and rates between 2 assets
// It returns the exchange price and rates
export const getPrice = (amount, data) => {
  const result1 = data[0];
  const result2 = data[1];

  const token1 = result1.answer;
  const token2 = result2.answer;

  const rates = token1 * decimalFactor / token2;
  const price = amount  * rates;

  return { price: price, rates: rates};
};
