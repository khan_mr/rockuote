import Web3 from "web3";
import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";

// This function returns current network name
export const getNetworkName = (num) => {
  switch(num){
    case 1:
      return "Mainnet";
    case 2:
      return "Morden";
    case 3:
      return "Ropsten";
    case 4:
      return "Rinkeby";
    case 42:
      return "Kovan";
    default:
      return "Undefined";
  }
}

// This function returns provider options for Web3Modal
export const getProviderOptions = () => {
  const providerOptions = {
    walletconnect: {
      package: WalletConnectProvider,
      options: {
        infuraId: process.env.REACT_APP_INFURA_ID,
      },
    },
  };
  return providerOptions;
};

// This function creates an instance of web3 based on the provider used
export const initWeb3 = (provider) => {
  const web3 = new Web3(provider);

  web3.eth.extend({
    methods: [
      {
        name: "chainId",
        call: "eth_chainId",
        outputFormatter: web3.utils.hexToNumber,
      },
    ],
  });

  return web3;
};

// This function connects to Provider wallet
// It creates and stores web3 instance in the context, and updates address state
export const getWeb3 = async () => {
  const web3Modal = new Web3Modal({
    network: "kovan",
    cacheProvider: false,
    providerOptions: getProviderOptions(), // Add other wallets besides Metamask
  });
  const provider = await web3Modal.connect();
  if (provider) {
    const web3 = initWeb3(provider);
    return web3;
  }
};

// This function returns the balance of ERC20 token of the connected account address
export const getTokenBalance = async (
  web3,
  accountAddress,
  contractAddress
) => {
  let minABI = [
    // balanceOf
    {
      constant: true,
      inputs: [{ name: "_owner", type: "address" }],
      name: "balanceOf",
      outputs: [{ name: "balance", type: "uint256" }],
      type: "function",
    },
    // decimals
    {
      constant: true,
      inputs: [],
      name: "decimals",
      outputs: [{ name: "", type: "uint8" }],
      type: "function",
    },
  ];

  try {
    let balance = null;
    if (contractAddress) {
      let contract = new web3.eth.Contract(minABI, contractAddress);
      balance = await contract.methods.balanceOf(accountAddress).call();
    } else {
      // If contract address is not specified, then return the ETH balance
      balance = await web3.eth.getBalance(accountAddress);
    }
    if (balance) return balance;
    else return 0;
  } catch(e) {
    console.log("get token balance error:", e);
    return 0;
  }
  
};
