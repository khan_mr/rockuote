// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "https://github.com/smartcontractkit/chainlink/blob/master/evm-contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
import {SafeMath} from "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/math/SafeMath.sol";
import {SafeCast} from "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/math/SafeCast.sol";

contract PriceFeed {
    uint256 private _decimalFactor = 10**uint256(18);   // To remove decimal

    /**
     * Returns the latest price of  Asset1/USD
     */
    function _getLatestPriceAsset1(address addr1) private view returns (int) {
        AggregatorV3Interface priceFeedAsset1 = AggregatorV3Interface(addr1);
        (,int price,,,) = priceFeedAsset1.latestRoundData();
        return price;
    }
    
    /**
     * Returns the latest price of Asset2/USD
     */
    function _getLatestPriceAsset2(address addr2) private view returns (int) {
        AggregatorV3Interface priceFeedAsset2 = AggregatorV3Interface(addr2);
        (,int price,,,) = priceFeedAsset2.latestRoundData();
        return price;
    }
    
    /**
     * Returns Asset1/Asset2 rates and price
     * The result is multiplied by 10^18 (to remove decimal places from data)
     */
    function getExchangePrice(uint256 amount, address addr1, address addr2) external view returns (uint256, uint256) {
        int asset1 = _getLatestPriceAsset1(addr1);
        int asset2 = _getLatestPriceAsset2(addr2);
        
        uint256 a1 = SafeCast.toUint256(asset1);
        uint256 a2 = SafeCast.toUint256(asset2);
        
        uint256 rates = SafeMath.div(a1 * _decimalFactor, a2);
        uint256 price = SafeMath.div(amount * rates, _decimalFactor);

        return(rates, price);
    }
  
}




